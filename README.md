### General
- Email handler Bundle

### Features & Capabilities
- SimpleMailerService the provides an overlay above the SwiftMailerBundle
- ReST end-points to send an email or resend an existing one
- Email document class for document persistence


### Setup
1- Update your composer.json to add ATS private packagist:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://packagist.ats-digital.com"
        }
    ],
}

```

2- Install the bundle in your application
```sh
$ php composer require ats/email-bundle
```

3- Register the bundle in the AppKernel
```php
<?php
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = [
            // ...
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle(),
            new ATS\CoreBundle\ATSCoreBundle(),
            new ATS\EmailBundle\ATSEmailBundle(),
            // ...
        ];
    }
    // ...
}
```

### Configuration

```yml
# Example configuration
ats_email:
    api_prefix: 'my-new-api-prefix'
    provider: swiftmailer
    provider_config:
        transport: gmail
        username: username@mail.com
        password: pa$$
```

_Note_: When the `api_prefix` is not given, a default (***api***) will be used instead


### Usage
- The bundle provides a *SimpleMailerService* and an *Email* document class as a super layer above the SwiftMailerBundle. Sending an email is then as simple as calling the *`send()`* method method from the SimpleMailerService class

```php
// php file

    $doSave = false;
    $email = new Email();
    $email->setTemplate("my-template.html.twig") // template file must be valid and accessible
          ->setMessageParameters($parameters) // $parameters is an associative array
          ->setSubject("Email subject")
          ->setFrom("sender@my-company.com") // Sender's address
          ->setFromName("John Doe")
          ->setTo(["recipient@whatever.com"])
          ->setCc(['copy@me.com', 'metoo@me.com'])
          ->setBcc(['blindCopy@me.com'])
          ->setAttatchments(['/path/to/attachment.ext']);
    $mailerService = $this->getContainer()->get('ats_email.service.simple_mailer');
    $mailerService->send($email, $doSave); // When the $doSave parameter is set to true, the email will be persisted in the DataBase
```

- The bundle also provides 2 end-points:
```
    POST /%api_prefix%/email/send/{doSave}
```
When set to true, the `doSave` parameter will save the email into the database after sending the email. When set to false, the controller action will only send an email to the specified recepient. Note that `doSave` defaults to `false`
```
    POST /%api_prefix%/email/resend/{id}
```
Where `id` is the MongId of the email object that needs to be resent

#### What's new?
[CHANGELOG.md](CHANGELOG)