<?php declare(strict_types=1);

namespace ATS\EmailBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;

/**
 * WebTestCase
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
abstract class WebTestCase extends BaseWebTestCase
{
}
