<?php
namespace ATS\EmailBundle\Tests\Unit\Document;

use ATS\EmailBundle\Document\Email;
use PHPUnit\Framework\TestCase;

/**
 * EmailTest
 *
 * @author Anis Ksontini <aksontini@ats-digital.com>
 */

class EmailTest extends TestCase
{
    public function testSetterGetterSentAt()
    {
        $email = new Email();

        $email->setFrom("me@mycompany.com")
            ->setFromName("me")
            ->setTo(["you@yourcompany.com"])
            ->setCc(["him@yourcompany.com"])
            ->setBcc(["her@yourcompany.com"])
            ->setSubject("Awesome Mailing")
            ->setTemplate("default")
            ->setMessageParameters([])
            ->setReplyTo("me@mycompany.com");

        $this->assertEquals("me", $email->getFromName());
        $this->assertEquals("me@mycompany.com", $email->getFrom());
        $this->assertEquals("you@yourcompany.com", $email->getTo()[0]);
        $this->assertEquals("him@yourcompany.com", $email->getCc()[0]);
        $this->assertEquals("her@yourcompany.com", $email->getBcc()[0]);
        $this->assertEquals("Awesome Mailing", $email->getSubject());
        $this->assertEquals("default", $email->getTemplate());
        $this->assertEquals("me@mycompany.com", $email->getReplyTo());
        $this->assertEquals(null, $email->getId());

        $email->addTo("new@to.com");
        $this->assertCount(2, $email->getTo());

        $email->addCc("Another@cc.com");
        $this->assertCount(2, $email->getCc());
        $this->assertTrue($email->hasCc());
        $email->setCc([]);
        $this->assertFalse($email->hasCc());

        $email->addBcc("Another@bcc.com");
        $this->assertCount(2, $email->getBcc());
        $this->assertTrue($email->hasBCc());
        $email->setBcc([]);
        $this->assertFalse($email->hasBcc());

        $this->assertCount(0, $email->getHeaders());
        $this->assertFalse($email->hasHeaders());
        $email->addHeaders('key', 'value');
        $this->assertCount(1, $email->getHeaders());
        $this->assertTrue($email->hasHeaders());
        $this->assertEquals(['key' => 'value'], $email->getHeaders());

    }

    public function testSetterGetterMessageParameters()
    {
        $email = new Email();
        $email->setMessageParameters(array('param' => "awsome"));

        $this->assertEquals(array('param' => "awsome", '_raw_' => ''), $email->getMessageParameters());
    }
}
