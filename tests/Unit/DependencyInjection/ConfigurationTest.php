<?php declare(strict_types=1);

namespace ATS\EmailBundle\Tests\Unit\DependencyIntection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use ATS\EmailBundle\DependencyInjection\Configuration;
use PHPUnit\Framework\TestCase;

/**
 * ConfigurationTest
 *
 * @author Wajih WERIEMI <wweriemi@ats-digital.com>
 */
class ConfigurationTest extends TestCase
{
    /**
     * Test suits for getConfigTreeBuilder
     *
     * @covers ATS\EmailBundle\DependencyInjection\Configuration::getConfigTreeBuilder
     */
    public function testGgetConfigTreeBuilder()
    {
        $configuration = new Configuration();
        $this->assertInstanceOf(TreeBuilder::class, $configuration->getConfigTreeBuilder());
    }
}
