<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Tests\Functional\Controller\Rest;

use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Tests\WebTestCase;
use ATS\EmailBundle\Manager\EmailManager;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;

/**
 * EmailControllerTest
 *
 * @author Anis Ksontini <aksontini@ats-digital.com>
 */
class EmailControllerTest extends WebTestCase
{
    protected $mailer;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EmailManager
     */
    private $emailManager;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        $this->emailManager = new EmailManager($this->container->get('doctrine_mongodb'));
        $this->emailManager->deleteAll();
    }

    /**
     * Test suits for SendEmailAction
     */
    public function testSendEmail()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer(), new DateTimeNormalizer()];

        $serializer = new Serializer($normalizers, $encoders);
        $email = new Email();
        $email->setFrom("me@mycompany.com")
            ->setFromName("me")
            ->setTo(["you@yourcompany.com"])
            ->setCc(["him@yourcompany.com"])
            ->setBcc(["her@yourcompany.com"])
            ->setSubject("Awesome Mailing")
            ->setTemplate("default")
            ->setMessageParameters([])
            ->setReplyTo("me@mycompany.com");

        $client = static::createClient();
        $client->enableProfiler();
        $content = $serializer->serialize($email, 'json');
        $client->request('POST', '/api/email/send', [], [], ['Content-Type' => 'application/json'], $content);
        $mailCollector = $client->getProfile()->getCollector('swiftmailer');

        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertSame(1, $mailCollector->getMessageCount());
        $collectedMessages = $mailCollector->getMessages();
        $message = $collectedMessages[0];
        $this->assertEquals('Awesome Mailing', $message->getSubject());
        $this->assertEquals('', $message->getBody());
        $this->emailManager->deleteAll();
        $this->assertCount(0, $this->emailManager->getAll());
        $client->request('POST', '/api/email/send/true', [], [], ['Content-Type' => 'application/json'], $content);
        $this->assertTrue($client->getResponse()->isSuccessful());
        $allEmails = $this->emailManager->getAll();
        $this->assertCount(1, $allEmails);
        $this->assertEquals($allEmails[0]->getSubject(), 'Awesome Mailing');
    }

    public function testResendEmail()
    {
        $this->emailManager->deleteAll();
        $this->assertCount(0, $this->emailManager->getAll());
        $client = static::createClient();
        $email = new Email();
        $email->setSubject("Resend")->setTo(['test@test.com'])->setFrom('sender@test.com');
        $this->emailManager->update($email);
        $this->assertCount(1, $this->emailManager->getAll());
        $client->enableProfiler();
        $id = $email->getId();
        $client->request('POST', "/api/email/resend/$id");

        $this->assertTrue($client->getResponse()->isSuccessful());
        $client->request('POST', "/api/email/resend/SomeIdThatIsNotDefined");
        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    private function getEmailAsArray()
    {
        return $emailContent = [
            "subject" => "subject",
            "from" => "sender@toto",
            "from_name" => "sender Name",
            "to" => "recipient@toto",
            "template" => "empty.html.twig",
            "message_parameters" => ['test' => 1],
        ];
    }
}
