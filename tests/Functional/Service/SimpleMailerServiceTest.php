<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Tests\Functional\Service;

use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Service\SimpleMailerService;
use ATS\EmailBundle\Tests\WebTestCase;

/**
 * SimpleMailerServiceTest
 *
 * @author Anis Ksontini <sksontini@ats-digital.com>
 */
class SimpleMailerServiceTest extends WebTestCase
{

    /**
     * {@inheritDoc}
     */
    private $container;
    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $swiftMailer = $this->createMock(\Swift_Mailer::class);
        $swiftMailer->expects($this->any())
            ->method('send')
            ->willReturn(true);

        $this->container = self::$kernel->getContainer();
        $this->container->set('\Swift_Mailer', $swiftMailer);
        $this->client = static::createClient();
        $this->container = $this->client->getContainer();
        $this->client->enableProfiler();
    }

    /**
     * Test suits for Send
     */
    public function testSend()
    {
        $email = new Email();
        $email->setFrom("me@mycompany.com")
            ->setFromName("me")
            ->setTo(["you@yourcompany.com"])
            ->setCc(["him@yourcompany.com"])
            ->setBcc(["her@yourcompany.com"])
            ->setSubject("Awesome Mailing")
            ->setTemplate("default")
            ->setMessageParameters([])
            ->setReplyTo("me@mycompany.com");

        $service = $this->container->get('ats_email.service.simple_mailer');
        $this->assertEquals(true, $service->send($email, false));
    }

    public function testRender()
    {
        $mailer = $this->container->get('ats_email.service.simple_mailer');

        $class = new \ReflectionClass(SimpleMailerService::class);
        $method = $class->getMethod('render');
        $method->setAccessible(true);
        $rendered = $method->invokeArgs($mailer, ["empty.html.twig", ['_raw_' => null]]);
        $this->assertEquals("", $rendered);
        $rendered = $method->invokeArgs($mailer, ["empty.html.twig", ['_raw_' => null]]);
        $this->assertEquals(null, $rendered);
    }
}
