<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Controller;

use ATS\CoreBundle\Controller\Rest\RestControllerTrait;
use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Service\SimpleMailerService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class EmailController extends Controller
{
    use RestControllerTrait;

    /**
     * @param Request             $request
     * @param bool                $doSave
     * @param SerializerInterface $serializer
     * @param SimpleMailerService $mailer
     *
     * @return Response
     */
    public function sendEmailAction(
        Request $request,
        bool $doSave,
        SerializerInterface $serializer,
        SimpleMailerService $mailer
    ) {
        $response = 0;
        /** @var Email $email */
        $email = $serializer->deserialize($request->getContent(), Email::class, 'json');
        if ($email !== 0) {
            $response = $mailer->send($email, $doSave);
        }

        return $this->renderResponse($response > 0);
    }

    /**
     *
     * @param Request             $request
     * @param string              $id
     * @param SimpleMailerService $mailer
     *
     * @return Response
     */
    public function resendEmailAction(Request $request, string $id, SimpleMailerService $mailer)
    {
        return new JsonResponse($mailer->resend($id));
    }
}
