<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Command;

use ATS\EmailBundle\DependencyInjection\Configuration;
use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Service\MailJetService;
use ATS\EmailBundle\Service\SimpleMailerService;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName("ats:email:send-email")
            ->setDescription("Send an email via a given prodiver")
            ->addArgument('provider', InputArgument::REQUIRED, 'Mailer service to be used')
            ->addOption('from', 'f', InputOption::VALUE_REQUIRED, 'From address')
            ->addOption('to', 't', InputOption::VALUE_REQUIRED, 'to address')
            ->addOption('body', 'b', InputOption::VALUE_REQUIRED, 'Email body');
    }

    public function isEnabled()
    {
        return $this->getContainer()->getParameter("dev_mode") === true;
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var string $provider */
        $provider = $input->getArgument('provider');
        /** @var string $from */
        $from = $input->getOption('from');
        /** @var string $to */
        $to = $input->getOption('to');
        /** @var string $body */
        $body = $input->getOption('body');

        $mailer = null;

        switch ($provider) {
            case Configuration::SWIFTMAILER:
                $mailer = $this->getContainer()->get(SimpleMailerService::class);
                break;
            case Configuration::MAIL_JET:
                $mailer = $this->getContainer()->get(MailJetService::class);
                break;
            default:
                throw new \Exception("Unknown provider $provider");
        }

        $email = new Email();
        $email->setFrom($from);
        $email->setTo([$to]);
        $email->setRawMessage($body);

        $mailer->send($email);
    }
}
