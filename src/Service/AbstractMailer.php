<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Service;

use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Event\EmailSentEvent;
use ATS\EmailBundle\Manager\EmailManager;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Templating\EngineInterface;

abstract class AbstractMailer
{
    protected const FORMAT_HTML = 'html';
    protected const FORMAT_PLAIN = 'text';

    /**
     * @var EngineInterface
     */
    protected $templatingEngine;

    /**
     * @var EmailManager
     */
    protected $emailManager;

    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return void
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param EngineInterface $templatingEngine
     *
     * @return void
     */
    public function setTemplatingEngine(EngineInterface $templatingEngine)
    {
        $this->templatingEngine = $templatingEngine;
    }

    /**
     * @param EmailManager $emailManager
     *
     * @return void
     */
    public function setEmailManager(EmailManager $emailManager)
    {
        $this->emailManager = $emailManager;
    }

    /**
     * Sends an email
     *
     * @param Email $email
     * @param boolean $save
     *
     * @return bool
     */
    public function send(Email $email, bool $save = false): bool
    {
        $status = $this->doSend($email);

        if ($status === true) {
            if ($save === true) {
                $this->emailManager->update($email);
            }

            $this->eventDispatcher->dispatch(EmailSentEvent::NAME, new EmailSentEvent());

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function resend(string $id): bool
    {
        $email = $this->emailManager->getOneBy(['id' => $id]);

        if ($email !== null) {
            return $this->send($email);
        }

        return false;
    }

    /**
     *
     * @param Email $email
     *
     * @return bool
     */
    abstract protected function doSend(Email $email): bool;

    /**
     * Renders a twig template with the passed in parameters array
     *
     * @param string $templateName
     * @param array  $parameters
     *
     * @return null|string
     */
    protected function render(string $templateName, $parameters = []): ?string
    {
        try {
            return $this->templatingEngine->render($templateName, $parameters);
        } catch (\RuntimeException $e) {
            return null;
        }
    }
}
