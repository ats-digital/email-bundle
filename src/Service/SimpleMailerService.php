<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Service;

use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Service\AbstractMailer;

class SimpleMailerService extends AbstractMailer
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function setMailer(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Email $email
     *
     * @return bool
     */
    protected function doSend(Email $email): bool
    {

        $message = (new \Swift_Message())
            ->setSubject($email->getSubject())
            ->setFrom($email->getFrom(), $email->getFromName())
            ->setTo($email->getTo())
            ->setBody(
                $this->render($email->getTemplate(), $email->getMessageParameters()),
                'text/html'
            );

        if ($email->hasCc() === true) {
            $message->setCc($email->getCc());
        }

        if ($email->hasBcc() === true) {
            $message->setBcc($email->getBcc());
        }

        if ($email->getReplyTo() !== null) {
            $message->setReplyTo($email->getReplyTo());
        }

        if ($email->hasAttachments() === true) {
            foreach ($email->getAttachments() as $path) {
                $message->attach(\Swift_Attachment::fromPath($path));
            }
        }

        $sentNumber = $this->mailer->send($message);

        return (bool) $sentNumber;
    }
}
