<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Service;

use ATS\EmailBundle\Document\Email;
use ATS\EmailBundle\Service\AbstractMailer;
use \Mailjet\Client;
use \Mailjet\Resources;

class MailJetService extends AbstractMailer
{
    const API_VERSION = 'v3.1';

    private $client;

    public function __construct(string $publicKey, string $privateKey)
    {
        if (class_exists(\Mailjet\Client::class) === true) {
            $this->client = new Client($publicKey, $privateKey, true, ['version' => self::API_VERSION]);
        }
    }

    protected function doSend(Email $email): bool
    {
        /** @var string $content */
        $content = $this->render($email->getTemplate(), $email->getMessageParameters());

        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => $email->getFrom(),
                        'Name' => $email->getFromName(),
                    ],
                    'To' => array_map(
                        function ($elem) {
                            return ['Email' => $elem];
                        },
                        $email->getTo()
                    ),
                    'Cc' => array_map(
                        function ($elem) {
                            return ['Email' => $elem];
                        },
                        $email->getCc()
                    ),
                    'Bcc' => array_map(
                        function ($elem) {
                            return ['Email' => $elem];
                        },
                        $email->getBcc()
                    ),
                    'Subject' => $email->getSubject(),
                    'TextPart' => (new \Html2Text\Html2Text($content))->getText(),
                    'HTMLPart' => $content,
                ],
            ],
        ];

        $response = $this->client->post(Resources::$Email, ['body' => $body]);

        return $response->success();
    }
}
