<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="ATS\EmailBundle\Repository\EmailRepository")
 */
class Email
{
    const DEFAULT_TEMPLATE = "ATSEmailBundle::empty.html.twig";

    /**
     * @var \MongoId
     * @ODM\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $subject;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $from;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $fromName;

    /**
     * @var array
     * @ODM\Field(type="collection")
     */
    protected $to;

    /**
     * @var array
     * @ODM\Field(type="collection")
     */
    protected $cc;

    /**
     * @var array
     * @ODM\Field(type="collection")
     */
    protected $bcc;

    /**
     * @var ?string
     * @ODM\Field(type="string")
     */
    protected $replyTo;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    protected $template;

    /**
     * @var array
     * @ODM\Field(type="hash")
     */
    protected $messageParameters;

    /**
     * @var array
     * @ODM\Field(type="hash")
     */
    protected $headers;

    /**
     *
     * @var array
     * @ODM\Field(type="collection")
     */
    protected $attachments;

    /**
     * Not Doctrine Managed
     *
     * @var string
     */
    protected $rawMessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->subject = '';
        $this->fromName = '';
        $this->rawMessage = '';
        $this->messageParameters = [];
        $this->to = [];
        $this->cc = [];
        $this->bcc = [];
        $this->headers = [];
        $this->attachments = [];
        $this->template = self::DEFAULT_TEMPLATE;
    }

    /**
     * Get the value of id
     *
     * @return  \MongoId
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of subject
     *
     * @return  string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * Set the value of subject
     *
     * @param  string  $subject
     *
     * @return  self
     */
    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get the value of from
     *
     * @return  string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * Set the value of from
     *
     * @param  string  $from
     *
     * @return  self
     */
    public function setFrom(string $from): self
    {
        $this->from = $from;

        return $this;
    }

    /**
     * Get the value of fromName
     *
     * @return  string
     */
    public function getFromName(): string
    {
        return $this->fromName !== '' ? $this->fromName : $this->from;
    }

    /**
     * Set the value of fromName
     *
     * @param  string  $fromName
     *
     * @return  self
     */
    public function setFromName(string $fromName): self
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get the value of to
     *
     * @return  array
     */
    public function getTo(): array
    {
        return $this->to;
    }

    /**
     * Set the value of to
     *
     * @param  array  $to
     *
     * @return  self
     */
    public function setTo(array $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function addTo(string $newTo): self
    {
        if (in_array($newTo, $this->to) === false) {
            $this->to[] = $newTo;
        }

        return $this;
    }

    /**
     * Get the value of cc
     *
     * @return  array
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * Set the value of cc
     *
     * @param  array  $cc
     *
     * @return  self
     */
    public function setCc(array $cc)
    {
        $this->cc = $cc;

        return $this;
    }

    public function addCc(string $newCc): self
    {
        if (in_array($newCc, $this->cc) === false) {
            $this->cc[] = $newCc;
        }

        return $this;
    }

    public function hasCc(): bool
    {
        return count($this->cc) > 0;
    }

    /**
     * Get the value of bcc
     *
     * @return  array
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set the value of bcc
     *
     * @param  array  $bcc
     *
     * @return  self
     */
    public function setBcc(array $bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    public function addBcc(string $newBcc): self
    {
        if (in_array($newBcc, $this->bcc) === false) {
            $this->bcc[] = $newBcc;
        }

        return $this;
    }

    public function hasBcc(): bool
    {
        return count($this->bcc) > 0;
    }

    /**
     * Get the value of replyTo
     *
     * @return  string|null
     */
    public function getReplyTo(): ?string
    {
        return $this->replyTo;
    }

    /**
     * Set the value of replyTo
     *
     * @param  ?string  $replyTo
     *
     * @return  self
     */
    public function setReplyTo(?string $replyTo): self
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * Get the value of template
     *
     * @return  string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * Set the value of template
     *
     * @param  string  $template
     *
     * @return  self
     */
    public function setTemplate(string $template): self
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get the value of messageParameters
     *
     * @return  array
     */
    public function getMessageParameters(): array
    {
        return array_merge($this->messageParameters, ['_raw_' => $this->getRawMessage()]);
    }

    /**
     * Set the value of messageParameters
     *
     * @param  array  $messageParameters
     *
     * @return  self
     */
    public function setMessageParameters(array $messageParameters): self
    {
        $this->messageParameters = $messageParameters;

        return $this;
    }

    /**
     * Get the value of headers
     *
     * @return  array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * Set the value of headers
     *
     * @param  array  $headers
     *
     * @return  self
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    public function addHeaders(string $key, string $value): self
    {
        $this->headers[$key] = $value;

        return $this;
    }

    public function hasHeaders(): bool
    {
        return count($this->headers) > 0;
    }

    /**
     * Get the value of attachments
     *
     * @return  array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * Set the value of attachments
     *
     * @param  array  $attachments
     *
     * @return  self
     */
    public function setAttachments(array $attachments): self
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function addAttachment(string $path): self
    {
        if (in_array($path, $this->attachments) === false) {
            $this->attachments[] = $path;
        }

        return $this;
    }

    public function hasAttachments(): bool
    {
        return count($this->attachments) > 0;
    }

    /**
     * Get not Doctrine Managed
     *
     * @return  string
     */
    public function getRawMessage()
    {
        return $this->rawMessage;
    }

    /**
     * Set not Doctrine Managed
     *
     * @param  string  $rawMessage  Not Doctrine Managed
     *
     * @return  self
     */
    public function setRawMessage(string $rawMessage)
    {
        $this->rawMessage = $rawMessage;

        return $this;
    }
}
