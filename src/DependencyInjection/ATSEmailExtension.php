<?php declare (strict_types = 1);

namespace ATS\EmailBundle\DependencyInjection;

use ATS\EmailBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

/**
 * ATSEmailExtension
 *
 * @author Ali Turki <aturki@ats-digital.com>
 */
class ATSEmailExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $this->initDefaults($container);

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('api_prefix', $config['api_prefix']);

        $provider = $config['provider'];

        switch ($provider) {
            case Configuration::SWIFTMAILER:
                $this->handleSwiftMailerConfig($container, $config);
                break;
            case Configuration::MAIL_JET:
                if (class_exists(\Mailjet\Client::class) === false) {
                    throw new \Exception("mailjet/mailjet-apiv3-php is required for using MailJet API.\nPlease install it via composer require mailjet/mailjet-apiv3-php");
                }
                $this->handleMailJetConfig($container, $config);
                break;
            default:
                throw new \Exception('Invalid provider in configuration');
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    private function handleSwiftMailerConfig(ContainerInterface $container, array $config)
    {
        $transport = $config['provider_config']['transport'];
        if ($transport === 'gmail') {
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.host', 'smtp.gmail.com');
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.port', 465);
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.encryption', 'ssl');
        } else {
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.host', $config['provider_config']['host']);
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.port', $config['provider_config']['port']);
            $container->setParameter('swiftmailer.mailer.default.transport.smtp.encryption', $config['provider_config']['encryption']);
        }

        $container->setParameter('swiftmailer.mailer.default.transport.smtp.auth_mode', 'login');
        $container->setParameter('swiftmailer.mailer.default.transport.name', 'smtp');
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.password', $config['provider_config']['password']);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.username', $config['provider_config']['username']);
    }

    private function handleMailJetConfig(ContainerInterface $container, array $config)
    {
        $container->setParameter('mailjet.api_public_key', $config['provider_config']['api_public_key']);
        $container->setParameter('mailjet.api_private_key', $config['provider_config']['api_private_key']);
    }

    private function initDefaults(ContainerInterface $container)
    {
        $container->setParameter('api_prefix', Configuration::DEFAULT_API_PREFIX);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.host', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.port', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.encryption', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.host', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.port', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.auth_mode', null);
        $container->setParameter('swiftmailer.mailer.default.transport.name', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.password', null);
        $container->setParameter('swiftmailer.mailer.default.transport.smtp.username', null);
        $container->setParameter('mailjet.api_public_key', null);
        $container->setParameter('mailjet.api_private_key', null);
    }
}
