<?php declare (strict_types = 1);

namespace ATS\EmailBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration
 *
 * @author Ali Turki <aturki@ats-digital.com>
 */
class Configuration implements ConfigurationInterface
{
    const SWIFTMAILER = 'swiftmailer';
    const MAIL_JET = 'mail_jet';

    const DEFAULT_API_PREFIX = 'api';

    const AVAILABLE_PROVIDERS = [
        self::SWIFTMAILER,
        self::MAIL_JET
    ];

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('ats_email');
        $rootNode
            ->children()
                ->enumNode('provider')
                    ->values(self::AVAILABLE_PROVIDERS)
                    ->cannotBeEmpty()
                    ->isRequired()
                ->end()
                ->scalarNode('api_prefix')->defaultValue(self::DEFAULT_API_PREFIX)->end()
                ->arrayNode('provider_config')
                    ->children()
                        ->scalarNode('transport')->defaultValue(null)->end()
                        ->scalarNode('username')->defaultValue(null)->end()
                        ->scalarNode('password')->defaultValue(null)->end()
                        ->scalarNode('host')->defaultValue(null)->end()
                        ->scalarNode('port')->defaultValue(null)->end()
                        ->scalarNode('encryption')->defaultValue(null)->end()
                        ->scalarNode('api_public_key')->defaultValue(null)->end()
                        ->scalarNode('api_private_key')->defaultValue(null)->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
