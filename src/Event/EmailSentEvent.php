<?php declare (strict_types = 1);

namespace ATS\EmailBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class EmailSentEvent extends Event
{
    const NAME = 'ats_email.sent.event';

    /**
     * @var \DateTime
     */
    private $sentAt;

    public function __construct()
    {
        $this->sentAt = new \DateTime();
    }

    public function getSentAt(): \DateTime
    {
        return $this->sentAt;
    }
}
